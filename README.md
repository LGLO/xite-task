### Simple recommendation service

#### Running

`sbt run` to start server.
`Enter` to stop server.

`sbt test` to run tests. There are some high level for Spray routes with wired
service and some unit tests for validations and also actor tests with akka-test kit.

```
> curl -X POST -H 'content-type: application/json' localhost:8085/register -d '{"userName":"a","email":"john@doe.com","age":30,"gender":1}'
{"userId":1,"videoId":902028440766092532}%

> curl -X POST -H 'content-type: application/json' localhost:8085/action -d '{"userId":1, "videoId":902028440766092532, "actionId":1}'
{"userId":1,"videoId":872251228767581037}%

> curl -X POST -H 'content-type: application/json' localhost:8085/action -d '{"userId":1, "videoId":872251228767581037, "actionId":1}'
{"userId":1,"videoId":7245999734908819531}%

> curl -X POST -H 'content-type: application/json' localhost:8085/action -d '{"userId":1, "videoId":902028440766092532, "actionId":1}'
{"errors":["video does not correspond to last given"]}%

# validations:
> curl -X POST -H 'content-type: application/json' localhost:8085/register -d '{"userName":"a","email":"john@doe","age":0,"gender":4}'
{"errors":["email is not valid","age is not valid","gender is not valid"]}%   
```

#### Comments and excuses

I don't really know what I can write here because task was quite well defined.
I tried to obey. 
I added one more requirement for a case when someone want's to register a user again (identified by email).

Email regex is found somewhere over the Internet. Firstly I used one from
Play Framework source, but it wasn't really good, it allowed `abc@abc`.

Using `case class Something(value: String) extends AnyVal` is controversial,
sometimes it pays off, sometimes it's just burden. In such small project it's an
overkill but I didn't want to appear lazy.

I created more tests than usually, given high level tests are working
tests for `VideoRecommendationService` are a somehow extra.
Also `UserActor` is internal to `RecommendationsActor` but I've created these tests
because doing any akka after a year break was fun.

Honestly I don't work with Spray nor Cats on daily basis, so I don't know all the tricks.
This task was mostly working with documentation for me.
