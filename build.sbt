name := "xite-task"
organization := "scalac.io"
version := "1.0.0"

scalaVersion := "2.12.6"
scalacOptions += "-Ypartial-unification"

val akkaActorVersion = "2.5.13"
val akkaStreamVersion = "2.5.13"
val akkaHttpVersion = "10.1.3"
val catsVersion = "1.1.0"
val scalatestVersion = "3.0.5"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % akkaActorVersion,
  "com.typesafe.akka" %% "akka-stream" % akkaStreamVersion,
  "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
  "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion,
  "org.typelevel" %% "cats-core" % catsVersion,
  "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpVersion % Test,
  "com.typesafe.akka" %% "akka-testkit" % akkaActorVersion % Test,
  "org.scalatest" %% "scalatest" % scalatestVersion
)
