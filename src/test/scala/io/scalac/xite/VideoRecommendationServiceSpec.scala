package io.scalac.xite

import java.util.concurrent.TimeUnit

import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestKit, TestProbe}
import akka.util.Timeout
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{Matchers, WordSpecLike}

import scala.concurrent.ExecutionContext

class VideoRecommendationServiceSpec extends TestKit(ActorSystem("RecommendationsActorSpec"))
  with ImplicitSender
  with WordSpecLike
  with Matchers
  with ScalaFutures {

  val email = Email("jane@janes.com")
  val userName = UserName("Jane")
  val age = Age(31)
  val gender = Female
  val userId = UserId(333L)
  val videoId = VideoId(1000L)

  implicit val ec: ExecutionContext = system.dispatcher
  implicit val timeout: Timeout = Timeout(5, TimeUnit.SECONDS)

  trait SetUp {
    val probe = TestProbe()
    val service = new VideoRecommendationServiceImpl(probe.ref)
  }

  s"${classOf[VideoRecommendationServiceImpl].getSimpleName}" should {
    s"forward ${classOf[RegisterRequest].getSimpleName} to actor and pass response back" in new SetUp {
      val request = RegisterRequest(email, userName, age, gender)
      val response = service.register(request)

      probe.expectMsg(request)
      val registered = UserRegistered(userId, videoId)

      probe.reply(registered)
      response.futureValue shouldBe registered
    }

    s"forward ${classOf[ActionRequest].getSimpleName} to actor and pass response back" in new SetUp {
      val request = ActionRequest(userId, videoId, ActionId(3))
      val response = service.action(request)

      probe.expectMsg(request)
      val registered = NextVideo(userId, videoId)

      probe.reply(registered)
      response.futureValue shouldBe registered
    }
  }
}
