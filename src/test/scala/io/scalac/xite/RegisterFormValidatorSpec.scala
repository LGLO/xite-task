package io.scalac.xite

import cats.data.NonEmptyList
import cats.data.Validated.{Invalid, Valid}
import org.scalatest.{Matchers, WordSpec}

class RegisterFormValidatorSpec extends WordSpec
  with Matchers
  with RegisterFormValidator {

  s"${classOf[RegisterFormValidator].getSimpleName}" should {
    "accumulate all errors" in {
      val form = RegisterForm("Taz", "_oo_", 4, 0)
      val expectedErrors = NonEmptyList.of(
        InvalidEmail, InvalidAge, InvalidGender
      )
      validate(form) shouldBe Invalid(expectedErrors)
    }

    s"transform valid ${classOf[RegisterForm].getSimpleName} " +
      s"to ${classOf[RegisterRequest]}.getSimpleName" in {
      val form = RegisterForm("Tom Dumoulin", "tomdum@uci.ch", 27, 1)
      val expected = RegisterRequest(
        Email("tomdum@uci.ch"),
        UserName("Tom Dumoulin"),
        Age(27),
        Male
      )
      validate(form) shouldBe Valid(expected)
    }

    "allow age to be between 5 and 120 (inclusive)" in {
      validateAge(4) shouldBe Invalid(NonEmptyList.of(InvalidAge))
      validateAge(5) shouldBe Valid(Age(5))
      validateAge(120) shouldBe Valid(Age(120))
      validateAge(121) shouldBe Invalid(NonEmptyList.of(InvalidAge))
    }

    "allow gender to be 1 or 2" in {
      validateGender(0) shouldBe Invalid(NonEmptyList.of(InvalidGender))
      validateGender(1) shouldBe Valid(Male)
      validateGender(2) shouldBe Valid(Female)
      validateGender(3) shouldBe Invalid(NonEmptyList.of(InvalidGender))
    }

    "allow only valid emails" in {
      validateEmail("") shouldBe Invalid(NonEmptyList.of(InvalidEmail))
      validateEmail("@missing.local") shouldBe Invalid(NonEmptyList.of(InvalidEmail))
      validateEmail("missing.domain@") shouldBe Invalid(NonEmptyList.of(InvalidEmail))
      validateEmail("domain.too@short") shouldBe Invalid(NonEmptyList.of(InvalidEmail))
      validateEmail("no.spaces @only.com") shouldBe Invalid(NonEmptyList.of(InvalidEmail))
      validateEmail("happy@go.lucky") shouldBe Valid(Email("happy@go.lucky"))
    }
  }

}
