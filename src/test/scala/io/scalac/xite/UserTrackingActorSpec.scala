package io.scalac.xite

import java.util.concurrent.TimeUnit

import akka.actor.{ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit}
import org.scalatest.{Matchers, WordSpecLike}

import scala.concurrent.duration.FiniteDuration

class UserTrackingActorSpec extends TestKit(ActorSystem("UserTrackingActorSpec"))
  with ImplicitSender
  with WordSpecLike
  with Matchers {

  s"${classOf[UserTrackingActor].getSimpleName}" should {

    val userId = UserId(42L)
    val videos = IndexedSeq.tabulate(10)(i => VideoId(i * 10))

    "reply with user registered response for init request and then " +
      "reply with next video id for valid requests" in {
      val actor = system.actorOf(Props(new UserTrackingActor(userId, videos)))
      actor ! Init
      expectMsg(UserRegistered(userId, videos(0)))

      actor ! ActionRequest(userId, videos(0), ActionId(1))
      expectMsg(NextVideo(userId, videos(1)))

      // expect video ids in round-robin fashion
      for (i <- 1 to 30) {
        actor ! ActionRequest(userId, videos(i % 10), ActionId(1))
        expectMsg(NextVideo(userId, videos((i + 1) % 10)))
      }
    }

    "reply with error if video id don't match last" in {
      val actor = system.actorOf(Props(new UserTrackingActor(userId, videos)))
      actor ! Init
      expectMsg(UserRegistered(userId, videos(0)))

      actor ! ActionRequest(userId, videos(7), ActionId(1))
      expectMsg(InvalidVideoId)
    }

    "not reply if until init request is received" in {
      val actor = system.actorOf(Props(new UserTrackingActor(userId, videos)))
      actor ! ActionRequest(userId, videos(0), ActionId(1))
      expectNoMessage(FiniteDuration(100, TimeUnit.MILLISECONDS))

      actor ! Init
      expectMsg(UserRegistered(userId, videos(0)))

      actor ! ActionRequest(userId, videos(0), ActionId(1))
      expectMsg(NextVideo(userId, videos(1)))
    }

    "not crash on unexpected message" in {
      val actor = system.actorOf(Props(new UserTrackingActor(userId, videos)))
      actor ! Init
      expectMsg(UserRegistered(userId, videos(0)))

      actor ! "Not a poison pill but still unexpected!"
      expectNoMessage(FiniteDuration(100, TimeUnit.MILLISECONDS))

      actor ! ActionRequest(userId, videos(0), ActionId(1))
      expectMsg(NextVideo(userId, videos(1)))
    }

  }

}