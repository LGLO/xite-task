package io.scalac.xite

import java.util.concurrent.TimeUnit

import akka.actor.{ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit}
import org.scalatest.{Matchers, WordSpecLike}

import scala.concurrent.duration.FiniteDuration

class RecommendationsActorSpec extends TestKit(ActorSystem("RecommendationsActorSpec"))
  with ImplicitSender
  with WordSpecLike
  with Matchers {

  val email = Email("some@email.com")
  val userName = UserName("GI Joe")
  val age = Age(30)
  val gender = Male

  s"${classOf[RecommendationsActor].getSimpleName}" should {

    "reply with unknown user for action requests of unknown users" in {
      val actor = system.actorOf(Props(new RecommendationsActor))
      actor ! ActionRequest(UserId(42L), VideoId(0L), ActionId(1))
      expectMsg(UnknownUser)
    }

    "register an user and recommend first video then recommend next videos" in {
      val actor = system.actorOf(Props(new RecommendationsActor))
      actor ! RegisterRequest(email, userName, age, gender)
      val UserRegistered(userId, videoId) = expectMsgType[UserRegistered]

      actor ! ActionRequest(userId, videoId, ActionId(2))
      val NextVideo(_, videoId2) = expectMsgType[NextVideo]

      videoId shouldNot equal(videoId2)
    }

    "not crash and lose state because of unexpected message" in {
      val actor = system.actorOf(Props(new RecommendationsActor))
      actor ! RegisterRequest(email, userName, age, gender)
      val UserRegistered(userId, videoId) = expectMsgType[UserRegistered]

      actor ! "Unexpected message"
      expectNoMessage(FiniteDuration(100, TimeUnit.MILLISECONDS))

      actor ! ActionRequest(userId, videoId, ActionId(2))
      val NextVideo(_, videoId2) = expectMsgType[NextVideo]

      videoId shouldNot equal(videoId2)
    }

    "reply with invalid video id if request doesn't contain last video id" in {
      val actor = system.actorOf(Props(new RecommendationsActor))
      actor ! RegisterRequest(email, userName, age, gender)
      val UserRegistered(userId, _) = expectMsgType[UserRegistered]

      actor ! ActionRequest(userId, VideoId(2334L), ActionId(2))
      expectMsg(InvalidVideoId)
    }
  }
}
