package io.scalac.xite

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, StatusCodes}
import akka.http.scaladsl.server._
import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalatest.{Matchers, WordSpec}
import spray.json.{DefaultJsonProtocol, JsArray, JsNull, JsNumber, JsObject, JsString}

import scala.concurrent.Future

/**
  * High level tests for complete application sans HTTP layer.
  */
class RecommendationWebServiceSpec extends WordSpec
  with Matchers
  with ScalatestRouteTest
  with SprayJsonSupport
  with DefaultJsonProtocol {

  /** Set up with real service for high level tests. */
  trait SetUp {
    val videoService = VideoRecommendationServiceImpl.makeService()
    val service = new RecommendationWebService(videoService)
    val routes = service.routes
  }

  /** Set up with fake service to check how transformation of results works. */
  trait FakeSetUp {

    def registerResult: Future[RegisterUserResponse] =
      Future.failed(new RuntimeException("fakeRegister unimplemented"))

    def actionResult: Future[ActionResponse] =
      Future.failed(new RuntimeException("fakeAction unimplemented"))

    val fakeVideoService = new VideoRecommendationService {

      override def register(r: RegisterRequest) = registerResult

      override def action(r: ActionRequest) = actionResult
    }
    val service = new RecommendationWebService(fakeVideoService)
    val routes = service.routes
  }

  "/register" should {
    val path = "/register"
    "reject non-JSON requests" in new FakeSetUp {
      val nonJsonEntity = HttpEntity(ContentTypes.`text/plain(UTF-8)`, "not a JSON")
      Post(path, nonJsonEntity) ~> Route.seal(routes) ~> check {
        status shouldEqual StatusCodes.UnsupportedMediaType
      }
    }

    "reject JSON without required fields" in new FakeSetUp {
      val jsonButInvalid = JsObject("someRandomField" -> JsNull)
      Post(path, jsonButInvalid) ~> Route.seal(routes) ~> check {
        status shouldEqual StatusCodes.BadRequest
      }
    }

    "return fields validation errors" in new FakeSetUp {
      val formWithInvalidValues = JsObject(
        "userName" -> JsString("Joe Doe"),
        "email" -> JsString("invalid"),
        "age" -> JsNumber(321),
        "gender" -> JsNumber(3)
      )

      Post(path, formWithInvalidValues) ~> routes ~> check {
        status shouldEqual StatusCodes.BadRequest
        responseAs[JsObject] shouldEqual JsObject(
          "errors" -> JsArray(
            JsString("email is not valid"),
            JsString("age is not valid"),
            JsString("gender is not valid")
          )
        )
      }
    }

    "allow to register user and suggest first video" in new FakeSetUp {

      override def registerResult = Future.successful(UserRegistered(UserId(301), VideoId(42)))

      val form = JsObject(
        "userName" -> JsString("Jane Doe"),
        "email" -> JsString("jane.doe@xite.com"),
        "age" -> JsNumber(30),
        "gender" -> JsNumber(2)
      )

      Post(path, form) ~> routes ~> check {
        status shouldEqual StatusCodes.OK
        responseAs[JsObject] shouldEqual JsObject(
          "userId" -> JsNumber(301),
          "videoId" -> JsNumber(42)
        )
      }
    }

    //case for my made up requirement
    "present error if email is already registered" in new FakeSetUp {

      override def registerResult = Future.successful(UserAlreadyRegistered)

      val form = JsObject(
        "userName" -> JsString("Jane Doe"),
        "email" -> JsString("jane.doe@xite.com"),
        "age" -> JsNumber(30),
        "gender" -> JsNumber(2)
      )

      Post(path, form) ~> routes ~> check {
        status shouldEqual StatusCodes.Conflict
        responseAs[JsObject] shouldEqual JsObject(
          "errors" -> JsArray(JsString("email is already used"))
        )
      }
    }

  }

  "/action" should {
    val path = "/action"
    "reject non-JSON requests" in new FakeSetUp {
      val nonJsonEntity = HttpEntity(ContentTypes.`text/plain(UTF-8)`, "not a JSON")
      Post(path, nonJsonEntity) ~> Route.seal(routes) ~> check {
        status shouldEqual StatusCodes.UnsupportedMediaType
      }
    }

    "reject JSON without required fields" in new FakeSetUp {
      val jsonButInvalid = JsObject("someRandomField" -> JsNull)
      Post(path, jsonButInvalid) ~> Route.seal(routes) ~> check {
        status shouldEqual StatusCodes.BadRequest
      }
    }

    val validForm = JsObject(
      "userId" -> JsNumber(9797345L),
      "videoId" -> JsNumber(4324556L),
      "actionId" -> JsNumber(3)
    )

    "reject request when 'userId' is invalid" in new FakeSetUp {

      override def actionResult = Future.successful(UnknownUser)

      Post(path, validForm) ~> routes ~> check {
        status shouldEqual StatusCodes.BadRequest
        responseAs[JsObject] shouldEqual JsObject(
          "errors" -> JsArray(JsString("userId does not exist"))
        )
      }
    }

    "reject request when 'videoId' does not match last" in new FakeSetUp {
      override def actionResult = Future.successful(InvalidVideoId)

      Post(path, validForm) ~> routes ~> check {
        status shouldEqual StatusCodes.BadRequest
        responseAs[JsObject] shouldEqual JsObject(
          "errors" -> JsArray(JsString("video does not correspond to last given"))
        )
      }
    }

    "suggest next video when request was valid" in new FakeSetUp {
      override def actionResult = Future.successful(NextVideo(UserId(9797345L), VideoId(6454556L)))

      Post(path, validForm) ~> routes ~> check {
        status shouldEqual StatusCodes.OK
        responseAs[JsObject] shouldEqual JsObject(
          "userId" -> JsNumber(9797345L),
          "videoId" -> JsNumber(6454556L)
        )
      }
    }
  }

  s"${classOf[RecommendationWebService].getSimpleName}" should {
    "cover happy path" in new SetUp {

      //Register a user
      val registerForm = JsObject(
        "userName" -> JsString("Jane Doe"),
        "email" -> JsString("jane.doe@xite.com"),
        "age" -> JsNumber(30),
        "gender" -> JsNumber(2)
      )

      Post("/register", registerForm) ~> routes ~> check {
        status shouldEqual StatusCodes.OK
        val registrationResponse = responseAs[JsObject]
        val userId = registrationResponse.fields("userId").convertTo[Long]
        val videoId = registrationResponse.fields("videoId").convertTo[Long]

        // Ask for next video 10 times, last response should contain first received id (round robin).
        val seenVideoIds = (1 to 10).foldLeft(videoId :: Nil) {
          case (videoIds, i) =>
            val form = JsObject(
              "userId" -> JsNumber(userId),
              "videoId" -> JsNumber(videoIds.head),
              "actionId" -> JsNumber(i % 3 + 1)
            )
            Post("/action", form) ~> routes ~> check {
              status shouldEqual StatusCodes.OK
              val responseObj = responseAs[JsObject]
              val videoId = responseObj.fields("videoId").convertTo[Long]
              videoId :: videoIds
            }
        }
        seenVideoIds should have size 11
        seenVideoIds.head shouldEqual seenVideoIds.last
        seenVideoIds.toSet should have size 10

        // Check for non last videoId
        val formWithWrongVideoId = JsObject(
          "userId" -> JsNumber(userId),
          "videoId" -> JsNumber(seenVideoIds.tail.head),
          "actionId" -> JsNumber(1)
        )

        Post("/action", formWithWrongVideoId) ~> routes ~> check {
          status shouldEqual StatusCodes.BadRequest
          responseAs[JsObject] shouldEqual JsObject(
            "errors" -> JsArray(JsString("video does not correspond to last given"))
          )
        }

        // Check for wrong user
        val formWithWrongUserId = JsObject(
          "userId" -> JsNumber(userId - 1),
          "videoId" -> JsNumber(seenVideoIds.head),
          "actionId" -> JsNumber(1)
        )
        Post("/action", formWithWrongUserId) ~> routes ~> check {
          status shouldEqual StatusCodes.BadRequest
          responseAs[JsObject] shouldEqual JsObject(
            "errors" -> JsArray(JsString("userId does not exist"))
          )
        }
      }

    }
  }

}
