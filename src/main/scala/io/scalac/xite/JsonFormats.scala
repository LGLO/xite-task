package io.scalac.xite

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import cats.data.NonEmptyList
import spray.json.{DefaultJsonProtocol, JsArray, JsObject, JsString, RootJsonWriter}

trait JsonFormats extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val registerFormFormat = jsonFormat4(RegisterForm)
  implicit val registerWebResponseFormat = jsonFormat2(RegisterWebResponse)
  implicit val actionFormFormat = jsonFormat3(ActionForm)
  implicit val actionWebResponseFormat = jsonFormat2(ActionWebResponse)

  implicit val validationErrorsWriter: RootJsonWriter[NonEmptyList[ValidationError]] =
    obj => JsObject("errors" -> JsArray(obj.toList.toVector.map(e => JsString(e.message))))

}
