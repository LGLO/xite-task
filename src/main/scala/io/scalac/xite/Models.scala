package io.scalac.xite

// Some people like such type-safe wrappers, some do not.
// I think this trade-off pays better in big code bases.
case class Email(value: String) extends AnyVal

case class UserName(value: String) extends AnyVal

case class Age(value: Int) extends AnyVal


sealed trait Gender

case object Male extends Gender

case object Female extends Gender


case class UserId(value: Long) extends AnyVal

case class VideoId(value: Long) extends AnyVal

case class ActionId(value: Int) extends AnyVal


final case class RegisterForm(userName: String, email: String, age: Int, gender: Int)

final case class RegisterWebResponse(userId: Long, videoId: Long)


final case class ActionForm(userId: Long, videoId: Long, actionId: Int)

final case class ActionWebResponse(userId: Long, videoId: Long)
