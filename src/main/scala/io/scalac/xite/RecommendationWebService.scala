package io.scalac.xite

import akka.actor.ActorSystem
import akka.http.scaladsl.marshalling._
import akka.http.scaladsl.model.HttpResponse
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.server.Directives._
import akka.stream.ActorMaterializer
import cats.data.NonEmptyList
import cats.data.Validated.{Invalid, Valid}

import scala.concurrent.{ExecutionContext, Future}

class RecommendationWebService(videoService: VideoRecommendationService)(
  implicit system: ActorSystem,
  materializer: ActorMaterializer,
  ec: ExecutionContext
) extends JsonFormats
  with RegisterFormValidator {

  private implicit val validationErrorsMarshaller: ToResponseMarshaller[NonEmptyList[ValidationError]] =
    validationErrorsWriter.map(entity => HttpResponse(BadRequest, entity = entity))

  private def mkErrorNel(error: ValidationError): NonEmptyList[ValidationError] =
    NonEmptyList.of(error)

  private val marshallRegisterUserResponse: RegisterUserResponse => Future[HttpResponse] = {
    case UserRegistered(userId, videoId) =>
      val webResponse = RegisterWebResponse(userId.value, videoId.value)
      Marshal(OK -> webResponse).to[HttpResponse]
    case UserAlreadyRegistered =>
      Marshal(Conflict -> mkErrorNel(EmailIsAlreadyUsed)).to[HttpResponse]
  }

  private val marshallActionResponse: ActionResponse => Future[HttpResponse] = {
    case UnknownUser =>
      Marshal(BadRequest -> mkErrorNel(UserIdDoesNotExist)).to[HttpResponse]
    case InvalidVideoId =>
      Marshal(BadRequest -> mkErrorNel(VideoIdDoesNotMatch)).to[HttpResponse]
    case NextVideo(userId, videoId) =>
      Marshal(OK -> ActionWebResponse(userId.value, videoId.value)).to[HttpResponse]
  }
 
  val routes =
    path("register") {
      post {
        entity(as[RegisterForm]) { form =>
          validate(form) match {
            case Invalid(errors) =>
              complete(errors)
            case Valid(request) =>
              complete {
                videoService
                  .register(request)
                  .flatMap(marshallRegisterUserResponse)
              }
          }
        }
      }
    } ~
      path("action") {
        post {
          entity(as[ActionForm]) { form =>
            val actionRequest = ActionRequest(
              UserId(form.userId),
              VideoId(form.videoId),
              ActionId(form.actionId)
            )
            complete {
              videoService
                .action(actionRequest)
                .flatMap(marshallActionResponse)
            }
          }
        }
      }
}
