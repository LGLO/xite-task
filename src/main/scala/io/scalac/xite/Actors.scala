package io.scalac.xite

import akka.actor.{Actor, ActorRef, Props}

import scala.util.Random

// --- Actor's messages ---
final case class RegisterRequest(
  email: Email,
  userName: UserName,
  age: Age,
  gender: Gender
)

final case class ActionRequest(
  userId: UserId,
  videoId: VideoId,
  actionId: ActionId
)

/** [[UserTrackingActor]] first request to receive when no previous video was watched */
case object Init

// --- End actor's messages ---

/**
  * Serves [[RegisterRequest]] and [[ActionRequest]].
  * This implementation assumes running on single node.
  * To scale up simple modification can be made:
  * [[RecommendationsActor]] that works on shard 'K' of 'N' will register user ids U that U % N == K,
  * and HTTP layer can use with similar consistent hashing of [[ActionRequest]].
  */
class RecommendationsActor extends Actor {

  override def receive: Actor.Receive = behaviour(
    usersActors = Map.empty,
    registeredEmails = Set.empty,
    lastUserId = UserId(0L)
  )

  def behaviour(
    usersActors: Map[UserId, ActorRef],
    registeredEmails: Set[Email],
    lastUserId: UserId
  ): Actor.Receive = {
    case RegisterRequest(email, _, _, _) =>
      // Task does not specify what to with registration requests that contain same data.
      // I am making up requirements here. I will not allow to register same email twice.
      // I do not make up more requirements regarding user name, age and gender.
      if (registeredEmails.contains(email)) {
        sender() ! UserAlreadyRegistered
      } else {
        val nextId = UserId(lastUserId.value + 1L)
        val actor = context.actorOf(Props(UserTrackingActor.makeActor(nextId)))
        actor.forward(Init)
        context.become(
          behaviour(
            usersActors + (nextId -> actor),
            registeredEmails + email,
            nextId
          )
        )
      }
    case request@ActionRequest(userId, _, _) =>
      usersActors.get(userId) match {
        case None =>
          sender() ! UnknownUser
        case Some(userActor) =>
          userActor.forward(request)
      }

  }
}

/**
  * Keeps track of last video watched per user
  * and suggest next video id in round-robin manner.
  * [[ActionRequest]] must match last response from this actor.
  */
class UserTrackingActor(val userId: UserId, val videos: IndexedSeq[VideoId]) extends Actor {

  import UserTrackingActor._

  override def receive: Actor.Receive = {
    case Init =>
      sender() ! UserRegistered(userId, videos(0))
      context.become(behaviour(0))
  }

  def behaviour(lastWatchedIndex: Int): Actor.Receive = {
    case ActionRequest(_, videoId, _) =>
      if (videos(lastWatchedIndex) == videoId) {
        // Do something useful with 'actionId' - 2nd field of UserActorActionRequest
        val nextIndex = (lastWatchedIndex + 1) % VideosSize
        val nextToWatch = videos(nextIndex)
        sender() ! NextVideo(userId, nextToWatch)
        context.become(behaviour(nextIndex))
      } else {
        sender() ! InvalidVideoId
      }
  }

}

object UserTrackingActor {

  val rand = Random
  val VideosSize = 10

  def makeActor(userId: UserId): UserTrackingActor = {

    def randomVideoIds: IndexedSeq[VideoId] = {
      val videos = IndexedSeq.fill[Long](VideosSize)(rand.nextLong().abs).map(VideoId)
      // Very defensive against case when random gives duplicates
      if (videos.toSet.size == VideosSize) videos
      else randomVideoIds
    }

    new UserTrackingActor(userId, randomVideoIds)
  }
}
