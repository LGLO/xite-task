package io.scalac.xite

import java.util.concurrent.TimeUnit

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.pattern.ask
import akka.util.Timeout

import scala.concurrent.{ExecutionContext, Future}

sealed trait RegisterUserResponse

case object UserAlreadyRegistered extends RegisterUserResponse

final case class UserRegistered(
  userId: UserId,
  videoId: VideoId
) extends RegisterUserResponse


sealed trait ActionResponse

/** When [[UserId]] is not know in [[RecommendationsActor]] */
case object UnknownUser extends ActionResponse

/** When [[VideoId]] from [[ActionRequest]] does not match last watched video. */
case object InvalidVideoId extends ActionResponse

/** When request was correct, sends next video to watch */
final case class NextVideo(userId: UserId, videoId: VideoId) extends ActionResponse

trait VideoRecommendationService {

  def register(request: RegisterRequest): Future[RegisterUserResponse]

  def action(request: ActionRequest): Future[ActionResponse]
}

class VideoRecommendationServiceImpl(videoActor: ActorRef)
  (implicit ec: ExecutionContext, timeout: Timeout) extends VideoRecommendationService {

  override def register(request: RegisterRequest): Future[RegisterUserResponse] =
    (videoActor ? request).mapTo[RegisterUserResponse]

  override def action(request: ActionRequest): Future[ActionResponse] =
    (videoActor ? request).mapTo[ActionResponse]

}

object VideoRecommendationServiceImpl {

  def makeService()(
    implicit system: ActorSystem,
    ec: ExecutionContext
  ): VideoRecommendationService = {
    val actor = system.actorOf(Props(new RecommendationsActor))
    new VideoRecommendationServiceImpl(actor)(ec, Timeout(5, TimeUnit.SECONDS))
  }
}
