package io.scalac.xite

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer

import scala.io.StdIn

object Main extends App {
  implicit val system = ActorSystem("recommendations-system")
  implicit val materializer = ActorMaterializer()
  implicit val executionContext = system.dispatcher

  val port = 8085

  val server = new RecommendationWebService(VideoRecommendationServiceImpl.makeService())
  val bindingFuture = Http().bindAndHandle(server.routes, "localhost", port)

  println(s"Starting server at http://localhost:$port/\nPress RETURN to stop...")
  StdIn.readLine()
  bindingFuture
    .flatMap(_.unbind())
    .onComplete(_ => system.terminate())

}


