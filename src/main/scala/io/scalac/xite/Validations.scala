package io.scalac.xite

import cats.data.Validated._
import cats.data.{Validated, ValidatedNel}
import cats.implicits._

sealed trait ValidationError {
  def message: String
}

case object InvalidEmail extends ValidationError {
  def message = "email is not valid"
}

case object InvalidAge extends ValidationError {
  def message = "age is not valid"
}

case object InvalidGender extends ValidationError {
  def message = "gender is not valid"
}

case object VideoIdDoesNotMatch extends ValidationError {
  def message = "video does not correspond to last given"
}

case object UserIdDoesNotExist extends ValidationError {
  def message = "userId does not exist"
}

case object EmailIsAlreadyUsed extends ValidationError {
  def message = "email is already used"
}

trait RegisterFormValidator {

  type ValidationResult[A] = ValidatedNel[ValidationError, A]

  private val emailRegex = """(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])""".r

  def validateEmail(raw: String): ValidationResult[Email] = {
    if (raw.nonEmpty && emailRegex.findFirstMatchIn(raw).isDefined) Email(raw).valid
    else InvalidEmail.invalidNel
  }

  def validateAge(raw: Int): ValidationResult[Age] =
    if (5 <= raw && raw <= 120) Age(raw).valid
    else InvalidAge.invalidNel

  def validateGender(raw: Int): ValidationResult[Gender] =
    if (raw == 1) Male.valid
    else if (raw == 2) Female.valid
    else InvalidGender.invalidNel

  def validate(form: RegisterForm): ValidatedNel[ValidationError, RegisterRequest] =
    (validateEmail(form.email),
      Validated.Valid(UserName(form.userName)),
      validateAge(form.age),
      validateGender(form.gender)
    ).mapN(RegisterRequest)

}

object RegisterFormValidator extends RegisterFormValidator
